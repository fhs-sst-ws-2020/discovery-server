# Example multi-stage Dockerfile for Gradle and Java.
# In this example we use 2 stages:
#    - Build & Test: Builds our Java application using gradle and executes the unit tests.
#    - Runtime: Packages the application built in the previous stage together with the Java runtime.
# The advantage of this approach is that the final runtime image doesn't contain the build tools.
# Therefore, the image is smaller and more secure!

#################################################################
# Stage 1: BUILD & TEST                                         #
#################################################################
FROM gradle:6.7.0-jdk8 AS build
# do copy only necessary build files - otherwise e.g. changes
# to the Dockerfile results in a complete rebuild
COPY --chown=gradle:gradle build.gradle gradlew settings.gradle /app/
COPY --chown=gradle:gradle ./gradle /app/gradle
COPY --chown=gradle:gradle ./src /app/src

# set work dir and build
WORKDIR /app/
RUN gradle build --no-daemon

#################################################################
# Stage 2: RUNTIME                                              #
#################################################################
FROM openjdk:8-jre-alpine

# run as non-root user
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring

# get sources from build stage
COPY --from=build /app/build/libs/*.jar app.jar

# start app - exposing port 8080
ENTRYPOINT ["java", "-jar", "app.jar"]

